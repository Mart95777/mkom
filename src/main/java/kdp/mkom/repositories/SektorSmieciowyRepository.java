package kdp.mkom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kdp.mkom.model.SektorSmieciowy;

public interface SektorSmieciowyRepository extends JpaRepository<SektorSmieciowy, Long> {

}
