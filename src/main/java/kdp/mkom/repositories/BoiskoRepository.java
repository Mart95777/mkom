package kdp.mkom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kdp.mkom.model.Boisko;

public interface BoiskoRepository extends JpaRepository<Boisko, Long> {

}
