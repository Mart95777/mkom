package kdp.mkom.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import kdp.mkom.model.Droga;

public interface DrogaRepository extends JpaRepository<Droga, Long> {

}
