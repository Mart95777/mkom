package kdp.mkom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sektor_smieciowy")
public class SektorSmieciowy {
	
	@Id
	private Long lp;
	
	@Column(name="ulica1")
	private String ulica1;
	
	@Column(name="ulica2")
	private String ulica2;
	@Column(name="nr")
	private String nr;
	@Column(name="dzielnica")
	private String dzielnica;
	@Column(name="suche_pn")
	private Integer suche_pn;
	@Column(name="suche_wt")
	private Integer suche_wt;
	@Column(name="suche_sr")
	private Integer suche_sr;
	@Column(name="suche_czw")
	private Integer suche_czw;
	@Column(name="suche_pt")
	private Integer suche_pt;
	@Column(name="suche_so")
	private Integer suche_so;
	@Column(name="mokre_pn")
	private Integer mokre_pn;
	@Column(name="mokre_wt")
	private Integer mokre_wt;
	@Column(name="mokre_sr")
	private Integer mokre_sr;

	@Column(name="mokre_czw")
	private Integer mokre_czw;
	@Column(name="mokre_pt")
	private Integer mokre_pt;
	@Column(name="mokre_so")
	private Integer mokre_so;
	@Column(name="zmieszane_pn")
	private Integer zmieszane_pn;
	@Column(name="zmieszane_wt")
	private Integer zmieszane_wt;
	@Column(name="zmieszane_sr")
	private Integer zmieszane_sr;
	
	@Column(name="zmieszane_czw")
	private Integer zmieszane_czw;
	@Column(name="zmieszane_pt")
	private Integer zmieszane_pt;
	@Column(name="zmieszane_so")
	private Integer zmieszane_so;
	@Column(name="sektor")
	private Integer sektor;
	@Column(name="lati")
	private Float lati;
	@Column(name="longi")
	private Float longi;
	
	public Long getLp() {
		return lp;
	}
	public void setLp(Long lp) {
		this.lp = lp;
	}
	public String getUlica1() {
		return ulica1;
	}
	public void setUlica1(String ulica1) {
		this.ulica1 = ulica1;
	}
	public String getUlica2() {
		return ulica2;
	}
	public void setUlica2(String ulica2) {
		this.ulica2 = ulica2;
	}
	public String getNr() {
		return nr;
	}
	public void setNr(String nr) {
		this.nr = nr;
	}
	public String getDzielnica() {
		return dzielnica;
	}
	public void setDzielnica(String dzielnica) {
		this.dzielnica = dzielnica;
	}
	public Integer getSuche_pn() {
		return suche_pn;
	}
	public void setSuche_pn(Integer suche_pn) {
		this.suche_pn = suche_pn;
	}
	public Integer getSuche_wt() {
		return suche_wt;
	}
	public void setSuche_wt(Integer suche_wt) {
		this.suche_wt = suche_wt;
	}
	public Integer getSuche_sr() {
		return suche_sr;
	}
	public void setSuche_sr(Integer suche_sr) {
		this.suche_sr = suche_sr;
	}
	public Integer getSuche_czw() {
		return suche_czw;
	}
	public void setSuche_czw(Integer suche_czw) {
		this.suche_czw = suche_czw;
	}
	public Integer getSuche_pt() {
		return suche_pt;
	}
	public void setSuche_pt(Integer suche_pt) {
		this.suche_pt = suche_pt;
	}
	public Integer getSuche_so() {
		return suche_so;
	}
	public void setSuche_so(Integer suche_so) {
		this.suche_so = suche_so;
	}
	public Integer getMokre_pn() {
		return mokre_pn;
	}
	public void setMokre_pn(Integer mokre_pn) {
		this.mokre_pn = mokre_pn;
	}
	public Integer getMokre_wt() {
		return mokre_wt;
	}
	public void setMokre_wt(Integer mokre_wt) {
		this.mokre_wt = mokre_wt;
	}
	public Integer getMokre_sr() {
		return mokre_sr;
	}
	public void setMokre_sr(Integer mokre_sr) {
		this.mokre_sr = mokre_sr;
	}
	public Integer getMokre_czw() {
		return mokre_czw;
	}
	public void setMokre_czw(Integer mokre_czw) {
		this.mokre_czw = mokre_czw;
	}
	public Integer getMokre_pt() {
		return mokre_pt;
	}
	public void setMokre_pt(Integer mokre_pt) {
		this.mokre_pt = mokre_pt;
	}
	public Integer getMokre_so() {
		return mokre_so;
	}
	public void setMokre_so(Integer mokre_so) {
		this.mokre_so = mokre_so;
	}
	public Integer getZmieszane_pn() {
		return zmieszane_pn;
	}
	public void setZmieszane_pn(Integer zmieszane_pn) {
		this.zmieszane_pn = zmieszane_pn;
	}
	public Integer getZmieszane_wt() {
		return zmieszane_wt;
	}
	public void setZmieszane_wt(Integer zmieszane_wt) {
		this.zmieszane_wt = zmieszane_wt;
	}
	public Integer getZmieszane_sr() {
		return zmieszane_sr;
	}
	public void setZmieszane_sr(Integer zmieszane_sr) {
		this.zmieszane_sr = zmieszane_sr;
	}
	public Integer getZmieszane_czw() {
		return zmieszane_czw;
	}
	public void setZmieszane_czw(Integer zmieszane_czw) {
		this.zmieszane_czw = zmieszane_czw;
	}
	public Integer getZmieszane_pt() {
		return zmieszane_pt;
	}
	public void setZmieszane_pt(Integer zmieszane_pt) {
		this.zmieszane_pt = zmieszane_pt;
	}
	public Integer getZmieszane_so() {
		return zmieszane_so;
	}
	public void setZmieszane_so(Integer zmieszane_so) {
		this.zmieszane_so = zmieszane_so;
	}
	public Integer getSektor() {
		return sektor;
	}
	public void setSektor(Integer sektor) {
		this.sektor = sektor;
	}
	public Float getLati() {
		return lati;
	}
	public void setLati(Float lati) {
		this.lati = lati;
	}
	public Float getLongi() {
		return longi;
	}
	public void setLongi(Float longi) {
		this.longi = longi;
	}
	
	

}
