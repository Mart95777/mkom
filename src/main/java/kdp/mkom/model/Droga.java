package kdp.mkom.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="droga")
public class Droga {
	
	@Id
	private Long lp;
	
	@Column(name="nr_ciagu")
	private Integer nr_ciagu;
	
	@Column(name="kanalizacja_deszczowa")
	private Integer kanalizacja_deszczowa;
	
	@Column(name="kategorie_ulicy")
	private String kategorie_ulicy;
	
	@Column(name="zarzadca_drogi")
	private String zarzadca_drogi;
	
	@Column(name="ulica1")
	private String ulica1;
	
	@Column(name="ulica2")
	private String ulica2;
	
	@Column(name="rodzaj_ulicy")
	private Integer rodzaj_ulicy;
	
	@Column(name="dlugosc_ulicy")
	private String dlugosc_ulicy;
	
	@Column(name="dzielnica")
	private String dzielnica;
	
	@Column(name="akt_prawny")
	private String akt_prawny;
	
	@Column(name="ketimu")
	private String ketimu;
	
	@Column(name="ubezpieczenie")
	private Integer ubezpieczenie;
	
	@Column(name="nawierzchnia")
	private String nawierzchnia;

	@Column(name="lati")
	private Float lati;
	
	@Column(name="longi")
	private Float longi;
	
	@Column(name="polilinia")
	private String polilinia;
	
	@Transient
	private String parsedPolyline;

	public Long getLp() {
		return lp;
	}

	public void setLp(Long lp) {
		this.lp = lp;
	}

	public Integer getNr_ciagu() {
		return nr_ciagu;
	}

	public void setNr_ciagu(Integer nr_ciagu) {
		this.nr_ciagu = nr_ciagu;
	}

	public Integer getKanalizacja_deszczowa() {
		return kanalizacja_deszczowa;
	}

	public void setKanalizacja_deszczowa(Integer kanalizacja_deszczowa) {
		this.kanalizacja_deszczowa = kanalizacja_deszczowa;
	}

	public String getKategorie_ulicy() {
		return kategorie_ulicy;
	}

	public void setKategorie_ulicy(String kategorie_ulicy) {
		this.kategorie_ulicy = kategorie_ulicy;
	}

	public String getZarzadca_drogi() {
		return zarzadca_drogi;
	}

	public void setZarzadca_drogi(String zarzadca_drogi) {
		this.zarzadca_drogi = zarzadca_drogi;
	}

	public String getUlica1() {
		return ulica1;
	}

	public void setUlica1(String ulica1) {
		this.ulica1 = ulica1;
	}

	public String getUlica2() {
		return ulica2;
	}

	public void setUlica2(String ulica2) {
		this.ulica2 = ulica2;
	}

	public Integer getRodzaj_ulicy() {
		return rodzaj_ulicy;
	}

	public void setRodzaj_ulicy(Integer rodzaj_ulicy) {
		this.rodzaj_ulicy = rodzaj_ulicy;
	}

	public String getDlugosc_ulicy() {
		return dlugosc_ulicy;
	}

	public void setDlugosc_ulicy(String dlugosc_ulicy) {
		this.dlugosc_ulicy = dlugosc_ulicy;
	}

	public String getDzielnica() {
		return dzielnica;
	}

	public void setDzielnica(String dzielnica) {
		this.dzielnica = dzielnica;
	}

	public String getAkt_prawny() {
		return akt_prawny;
	}

	public void setAkt_prawny(String akt_prawny) {
		this.akt_prawny = akt_prawny;
	}

	public String getKetimu() {
		return ketimu;
	}

	public void setKetimu(String ketimu) {
		this.ketimu = ketimu;
	}

	public Integer getUbezpieczenie() {
		return ubezpieczenie;
	}

	public void setUbezpieczenie(Integer ubezpieczenie) {
		this.ubezpieczenie = ubezpieczenie;
	}

	public String getNawierzchnia() {
		return nawierzchnia;
	}

	public void setNawierzchnia(String nawierzchnia) {
		this.nawierzchnia = nawierzchnia;
	}

	public Float getLati() {
		return lati;
	}

	public void setLati(Float lati) {
		this.lati = lati;
	}

	public Float getLongi() {
		return longi;
	}

	public void setLongi(Float longi) {
		this.longi = longi;
	}

	public String getPolilinia() {
		return polilinia;
	}

	public void setPolilinia(String polilinia) {
		this.polilinia = polilinia;
	}

	public String getParsedPolyline() {
		return parsedPolyline;
	}

	public void setParsedPolyline(String parsedPolyline) {
		this.parsedPolyline = parsedPolyline;
	}

}
