package kdp.mkom.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="boisko")
public class Boisko {
	
	@Id
	private Long lp;
	
	@Column(name="typ")
	private String typ;
	
	@Column(name="przeznaczenie")
	private String przeznaczenie;
	
	@Column(name="dnitygodnia")
	private String dniTygodnia;
	
	@Column(name="lati")
	private Float lati;
	
	@Column(name="longi")
	private Float longi;
	
	@ManyToMany
	@JoinTable(name="placowka_boisko_cross", joinColumns = @JoinColumn(name="lp_boisko", referencedColumnName = "lp"),
		inverseJoinColumns = @JoinColumn(name = "lp_placowka", referencedColumnName = "lp"))
	private Set<Placowka> placowkaSet = new HashSet();

	public Long getLp() {
		return lp;
	}

	public void setLp(Long lp) {
		this.lp = lp;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getPrzeznaczenie() {
		return przeznaczenie;
	}

	public void setPrzeznaczenie(String przeznaczenie) {
		this.przeznaczenie = przeznaczenie;
	}

	public String getDniTygodnia() {
		return dniTygodnia;
	}

	public void setDniTygodnia(String dniTygodnia) {
		this.dniTygodnia = dniTygodnia;
	}

	public Float getLati() {
		return lati;
	}

	public void setLati(Float lati) {
		this.lati = lati;
	}

	public Float getLongi() {
		return longi;
	}

	public void setLongi(Float longi) {
		this.longi = longi;
	}

	public Set<Placowka> getPlacowkaSet() {
		return placowkaSet;
	}

	public void setPlacowkaSet(Set<Placowka> placowkaSet) {
		this.placowkaSet = placowkaSet;
	}

	
}
