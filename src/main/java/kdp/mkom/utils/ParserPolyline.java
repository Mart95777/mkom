package kdp.mkom.utils;

public class ParserPolyline {
	
	public static String parsePolyline(String sin){
		if(sin != null){
			String[] sarr = sin.split(";");
			StringBuilder strb = new StringBuilder("[");
			String[] pair = sarr[0].split(",");
			strb.append("{\"lat\": "+pair[0]+", \"lng\": "+pair[1]+"}");
			for(int i=1;i<sarr.length;i++){
				pair = sarr[i].split(",");
				strb.append(",{\"lat\": "+pair[0]+", \"lng\": "+pair[1]+"}");
			}
			strb.append("]");
			String sout = strb.toString();
			return sout;
		}else{
			return "";
		}
	}

}
