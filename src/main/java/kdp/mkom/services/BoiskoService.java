package kdp.mkom.services;

import kdp.mkom.model.Boisko;

public interface BoiskoService {
	Iterable<Boisko> listAllBoisko();
	Boisko saveBoisko(Boisko boisko);
	Boisko listBoiskoByLp(Long lp);

}
