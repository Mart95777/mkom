package kdp.mkom.services;

import kdp.mkom.model.Droga;

public interface DrogaService {
	
	Iterable<Droga> listAllDroga();
	Droga saveDroga(Droga droga);
	Droga listDrogaByLp(Long lp);

}
