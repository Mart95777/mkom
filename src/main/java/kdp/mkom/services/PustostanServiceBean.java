package kdp.mkom.services;

import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdp.mkom.model.Pustostan;
import kdp.mkom.repositories.PustostanRepository;

@Service
public class PustostanServiceBean implements PustostanService {
	
	@PersistenceContext
    private EntityManager em;
	private PustostanRepository pustostanRepository;
	private Iterable<Pustostan> pustostany;
	
	private Logger log = Logger.getLogger(PustostanServiceBean.class);
	
	@Autowired
	public void setPustostanRepository(PustostanRepository pustostanRepository) {
		this.pustostanRepository = pustostanRepository;
	}

	@Override
	public Iterable<Pustostan> listAllPustostan() {
		return pustostanRepository.findAll();
	}
	
	@Override
	public Pustostan  listPustostanByLp(Long lp) {
		Pustostan item = pustostanRepository.getOne(lp);
		item.setLati(item.getDzialkamiejska().getLati());
		item.setLongi(item.getDzialkamiejska().getLongi());
		return item;
	}

	@Override
	public Pustostan savePustostan(Pustostan pustostan) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Pustostan> listAllPustostanDlaMapy() {
		pustostany = listAllPustostan();
		for(Pustostan pustostan : pustostany){
			if(pustostan.getDzialkamiejska() != null){
				pustostan.setLati(pustostan.getDzialkamiejska().getLati());
				pustostan.setLongi(pustostan.getDzialkamiejska().getLongi());
			}
		}
		return pustostany;
	}
	
	@Override
	public Iterable<Pustostan> listPustostanByBOM(Integer bom){
		TypedQuery<Pustostan> query = em.createQuery("from Pustostan p where p.bom = ?1", Pustostan.class);
		query.setParameter(1, bom);
		pustostany = query.getResultList();
		Iterator<Pustostan> iterator = pustostany.iterator();
		while (iterator.hasNext()) {
			Pustostan next = iterator.next();
            if (next.getDzialkamiejska() != null) {
                next.setLati(next.getDzialkamiejska().getLati());
                next.setLongi(next.getDzialkamiejska().getLongi());
            }
        }
        return pustostany;
	}

}
