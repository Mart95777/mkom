package kdp.mkom.services;



import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdp.mkom.model.Boisko;
import kdp.mkom.repositories.BoiskoRepository;

@Service
public class BoiskoServiceBean implements BoiskoService {
	
	
	private BoiskoRepository boiskoRepository;
	
	private Logger log = Logger.getLogger(BoiskoServiceBean.class);

	@Override
	public Iterable<Boisko> listAllBoisko() {
		return boiskoRepository.findAll();
	}

	@Override
	public Boisko saveBoisko(Boisko boisko) {
		return null;
	}

	@Override
	public Boisko listBoiskoByLp(Long lp) {
		return boiskoRepository.findOne(lp);
	}
	
	@Autowired
	public void setBoiskoRepository(BoiskoRepository boiskoRepository) {
		this.boiskoRepository = boiskoRepository;
	}
	
	

}
