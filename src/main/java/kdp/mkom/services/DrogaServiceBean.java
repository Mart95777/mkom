package kdp.mkom.services;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdp.mkom.model.Droga;
import kdp.mkom.repositories.DrogaRepository;
import kdp.mkom.utils.ParserPolyline;

@Service
public class DrogaServiceBean implements DrogaService {
	
	private DrogaRepository drogaRepository;
	
	private Logger log = Logger.getLogger(DrogaServiceBean.class);
	
	@Autowired
	public void setDrogaRepository(DrogaRepository drogaRepository) {
		this.drogaRepository = drogaRepository;
	}

	@Override
	public Iterable<Droga> listAllDroga() {
		Iterable<Droga> drogaList = new ArrayList<Droga>(drogaRepository.findAll());
		for (Droga d : drogaList){
			d.setParsedPolyline(ParserPolyline.parsePolyline(d.getPolilinia()));
		}
		return drogaList;
	}

	@Override
	public Droga saveDroga(Droga droga) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Droga listDrogaByLp(Long lp) {
		Droga item = drogaRepository.findOne(lp);
		item.setParsedPolyline(ParserPolyline.parsePolyline(item.getPolilinia()));
		return item;
	}
	
	

}
