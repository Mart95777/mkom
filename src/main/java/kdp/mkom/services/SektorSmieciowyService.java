package kdp.mkom.services;

import kdp.mkom.model.SektorSmieciowy;

public interface SektorSmieciowyService {
	
	Iterable<SektorSmieciowy> listAllSektorSmieciowy();
	SektorSmieciowy saveSektorSmieciowy(SektorSmieciowy sektorSmieciowy);
	SektorSmieciowy listSektorSmieciowyByLp(Long lp);

}
