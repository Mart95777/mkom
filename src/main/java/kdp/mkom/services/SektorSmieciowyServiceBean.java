package kdp.mkom.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kdp.mkom.model.SektorSmieciowy;
import kdp.mkom.repositories.SektorSmieciowyRepository;

@Service
public class SektorSmieciowyServiceBean implements SektorSmieciowyService {
	
	private SektorSmieciowyRepository sektorSmieciowyRepository;

	@Autowired
	public void setSektorSmieciowyRepository(SektorSmieciowyRepository sektorSmieciowyRepository) {
		this.sektorSmieciowyRepository = sektorSmieciowyRepository;
	}

	@Override
	public Iterable<SektorSmieciowy> listAllSektorSmieciowy() {
		return sektorSmieciowyRepository.findAll();
	}

	@Override
	public SektorSmieciowy saveSektorSmieciowy(SektorSmieciowy sektorSmieciowy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SektorSmieciowy listSektorSmieciowyByLp(Long lp) {
		return sektorSmieciowyRepository.getOne(lp);
	}

}
